import pytest

from state import black, white


def test_white_has_15_checkers_at_beginning(initial_board):
    initial_board.turn = white
    assert initial_board.number_of_checkers() == 15


def test_black_has_15_checkers_at_beginning(initial_board):
    initial_board.turn = black
    assert initial_board.number_of_checkers() == 15


def test_pip_count_at_beginning(initial_board):
    assert initial_board.pip_count(white) == 167
    assert initial_board.pip_count(black) == 167


def test_bearing_off_not_possible_for_white(initial_board):
    initial_board.turn = white
    for checker in initial_board.all_checkers():
        assert initial_board.can_bear_off(checker) is False


def test_bearing_off_not_possible_for_black(initial_board):
    initial_board.turn = black
    for checker in initial_board.all_checkers():
        assert initial_board.can_bear_off(checker) is False


def test_is_blocked_for_white(initial_board):
    initial_board.turn = white
    assert initial_board.point_blocked(24) == True
    assert initial_board.point_blocked(18) == False


def test_is_blocked_for_black(initial_board):
    initial_board.turn = black
    assert initial_board.point_blocked(12) == True
    assert initial_board.point_blocked(4) == False


def test_legal_move_with_5_for_white(initial_board):
    initial_board.turn = white
    assert initial_board.legal_move((1, 6)) is False
    assert initial_board.legal_move((12, 17)) is True


def test_legal_move_with_5_for_black(initial_board):
    initial_board.turn = black
    assert initial_board.legal_move((6, 1)) is False
    assert initial_board.legal_move((13, 8)) is True


def test_legal_move_with_1_for_white(initial_board):
    initial_board.turn = white
    assert initial_board.legal_move((12, 13)) is False
    assert initial_board.legal_move((19, 20)) is True


def test_legal_move_with_1_for_black(initial_board):
    initial_board.turn = black
    assert initial_board.legal_move((13, 12)) is False
    assert initial_board.legal_move((24, 23)) is True


def test_no_winner_yet(initial_board):
    assert initial_board.is_won() is False


def test_winning_condition_for_white(empty_board):
    board = empty_board
    board.points[5] = -1
    board.checkers['black'] = [1, 5]
    assert board.is_won() is True


def test_winning_condition_black(empty_board):
    board = empty_board
    board.points[5] = 1
    board.checkers['white'] = [1, 5]
    assert board.is_won() is True


def test_no_race_position(empty_board):
    board = empty_board
    board.points[2] = -3
    board.points[12] = 1
    board.points[13] = -1
    board.points[24] = 2
    board.checkers['white'] = [2, 12, 24]
    board.checkers['black'] = [2, 13, 2]
    assert board.is_race() is False


def test_race_position(empty_board):
    board = empty_board
    board.points[2] = -3
    board.points[12] = -1
    board.points[13] = 1
    board.points[24] = 2
    board.checkers['white'] = [2, 13, 24]
    board.checkers['black'] = [2, 12, 2]
    assert board.is_race() is True


def test_apply_move_6_for_white(initial_board):
    board = initial_board
    board.turn = white
    move = (1, 7)
    board.apply_move(move)
    assert board.points[1] == 1
    assert board.points[7] == 1
    assert board.checkers['white'] == [5, 1, 7, 12, 17, 19]


def test_apply_move_2_for_black(initial_board):
    board = initial_board
    board.turn = black
    move = (24, 22)
    board.apply_move(move)
    assert board.points[24] == -1
    assert board.points[22] == -1
    assert board.checkers['black'] == [5, 24, 22, 13, 8, 6]


def test_bar_move_5_for_white(empty_board):
    board = empty_board
    board.turn = white
    board.points[0] = 1
    board.checkers['white'] = [1, 0]
    move = (0, 5)
    board.apply_move(move)
    assert board.points[0] == 0
    assert board.points[5] == 1
    assert board.checkers['white'] == [1, 5]


def test_bar_move_3_for_black(empty_board):
    board = empty_board
    board.turn = black
    board.points[25] = -1
    board.checkers['black'] = [1, 25]
    move = (25, 22)
    board.apply_move(move)
    assert board.points[22] == -1
    assert board.points[25] == 0
    assert board.checkers['black'] == [1, 22]


def test_21_does_not_remove_first_checkers_element(initial_board):
    board = initial_board
    board.turn = black
    move = (6, 4)
    board.apply_move(move)
    assert board.points[6] == -4
    assert board.points[4] == -1
    assert board.checkers['black'] == [5, 24, 13, 8, 6, 4]
    move = (4, 3)
    board.apply_move(move)
    assert board.points[4] == 0
    assert board.points[3] == -1
    assert board.checkers['black'] == [5, 24, 13, 8, 6, 3]


def test_capturing_with_white(empty_board):
    board = empty_board
    board.points[8] = 2
    board.points[11] = -1
    board.checkers['white'] = [1, 8]
    board.checkers['black'] = [1, 11]
    board.turn = white
    move = (8, 11)
    board.apply_move(move)
    assert board.points[8] == 1
    assert board.points[11] == 1
    assert board.points[25] == -1
    assert board.checkers['white'] == [2, 8, 11]
    assert board.checkers['black'] == [1, 25]


def test_capturing_with_black(empty_board):
    board = empty_board
    board.points[13] = 1
    board.points[18] = -1
    board.points[24] = 1
    board.checkers['white'] = [2, 13, 24]
    board.checkers['black'] = [1, 18]
    board.turn = black
    move = (18, 13)
    board.apply_move(move)
    assert board.points[0] == 1
    assert board.points[13] == -1
    assert board.points[18] == 0
    assert board.checkers['black'] == [1, 13]
    assert board.checkers['white'] == [2, 0, 24]


def test_bearing_off_with_white(empty_board):
    board = empty_board
    board.points[24] = 2
    board.checkers['white'] = [1, 24]
    board.turn = white
    move = (24, 25)
    board.apply_move(move)
    assert board.points[24] == 1
    assert board.points[25] == 0
    assert board.checkers['white'] == [1, 24]


def test_bearing_off_with_black(empty_board):
    board = empty_board
    board.points[2] = -1
    board.checkers['black'] = [1, 2]
    board.turn = black
    move = (2, 0)
    board.apply_move(move)
    assert board.points[2] == 0
    assert board.points[0] == 0
    assert board.checkers['black'] == [0]


def test_model_after_capturing_and_bearing_off_with_white(empty_board):
    board = empty_board
    board.points[6] = -2
    board.points[14] = 1
    board.points[19] = 2
    board.points[20] = -1
    board.checkers['white'] = [2, 14, 19]
    board.checkers['black'] = [2, 20, 6]
    board.turn = white
    move1 = (14, 20)
    move2 = (20, 25)
    moves = (move1, move2)
    board.apply_moves(moves)
    assert board.points[20] == 0
    assert board.points[25] == -1
    assert board.checkers['white'] == [1, 19]
    assert board.checkers['black'] == [2, 25, 6]


def test_model_after_capturing_and_bearing_off_with_black(empty_board):
    board = empty_board
    board.points[4] = 1
    board.points[5] = -2
    board.points[16] = 1
    board.checkers['white'] = [2, 4, 16]
    board.checkers['black'] = [1, 5]
    board.turn = black
    move1 = (5, 4)
    move2 = (4, 0)
    moves = (move1, move2)
    board.apply_moves(moves)
    assert board.points[4] == 0
    assert board.points[0] == 1
    assert board.checkers['white'] == [2, 0, 16]
    assert board.checkers['black'] == [1, 5]
