import pytest

from state import black


def test_11(empty_board):
    board = empty_board
    board.points[24] = -1
    board.points[18] = -1
    board.points[12] = -1
    board.points[6] = -1
    board.checkers['black'] = [4, 24, 18, 12, 6]
    board.rolls = (1, 1, 1, 1)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 35
    assert ((24, 23), (23, 22), (22, 21), (21, 20)) in moves
    assert ((24, 23), (23, 22), (22, 21), (18, 17)) in moves
    assert ((24, 23), (23, 22), (18, 17), (17, 16)) in moves
    assert ((24, 23), (18, 17), (17, 16), (16, 15)) in moves
    assert ((24, 23), (23, 22), (22, 21), (12, 11)) in moves
    assert ((24, 23), (23, 22), (12, 11), (11, 10)) in moves
    assert ((24, 23), (23, 22), (22, 21), (6, 5)) in moves
    assert ((24, 23), (23, 22), (6, 5), (5, 4)) in moves
    assert ((24, 23), (23, 22), (12, 11), (6, 5)) in moves
    assert ((24, 23), (18, 17), (17, 16), (12, 11)) in moves
    assert ((24, 23), (18, 17), (17, 16), (6, 5)) in moves
    assert ((24, 23), (18, 17), (12, 11), (11, 10)) in moves
    assert ((24, 23), (18, 17), (12, 11), (6, 5)) in moves
    assert ((24, 23), (18, 17), (6, 5), (5, 4)) in moves
    assert ((24, 23), (12, 11), (11, 10), (10, 9)) in moves
    assert ((24, 23), (12, 11), (11, 10), (6, 5)) in moves
    assert ((24, 23), (12, 11), (6, 5), (5, 4)) in moves
    assert ((24, 23), (6, 5), (5, 4), (4, 3)) in moves
    assert ((18, 17), (17, 16), (16, 15), (15, 14)) in moves
    assert ((18, 17), (17, 16), (16, 15), (12, 11)) in moves
    assert ((18, 17), (17, 16), (12, 11), (11, 10)) in moves
    assert ((18, 17), (12, 11), (11, 10), (10, 9)) in moves
    assert ((18, 17), (17, 16), (16, 15), (6, 5)) in moves
    assert ((18, 17), (17, 16), (6, 5), (5, 4)) in moves
    assert ((18, 17), (17, 16), (12, 11), (6, 5)) in moves
    assert ((18, 17), (17, 16), (6, 5), (5, 4)) in moves
    assert ((18, 17), (12, 11), (11, 10), (10, 9)) in moves
    assert ((18, 17), (12, 11), (11, 10), (6, 5)) in moves
    assert ((18, 17), (12, 11), (6, 5), (5, 4)) in moves
    assert ((18, 17), (6, 5), (5, 4), (4, 3)) in moves
    assert ((12, 11), (11, 10), (10, 9), (9, 8)) in moves
    assert ((12, 11), (11, 10), (10, 9), (6, 5)) in moves
    assert ((12, 11), (11, 10), (6, 5), (5, 4)) in moves
    assert ((12, 11), (6, 5), (5, 4), (4, 3)) in moves
    assert ((6, 5), (5, 4), (4, 3), (3, 2)) in moves
