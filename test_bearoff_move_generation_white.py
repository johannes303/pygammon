from state import white


def test_41_and_not_all_at_home(empty_board):
    board = empty_board
    board.points[24] = 1
    board.points[21] = 1
    board.points[1] = 1
    board.checkers['white'] = [3, 1, 21, 24]
    board.rolls = (4, 1)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 2
    assert ((1, 5), (5, 6)) in moves
    assert ((1, 5), (21, 22)) in moves


def test_41_and_all_at_home(empty_board):
    board = empty_board
    board.points[24] = 1
    board.points[21] = 1
    board.checkers['white'] = [2, 21, 24]
    board.rolls = (4, 1)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 2
    assert ((21, 25), (24, 25)) in moves
    assert ((21, 22), (22, 25)) in moves


def test_51_when_checker_on_6(empty_board):
    board = empty_board
    board.points[19] = 1
    board.points[22] = 1
    board.points[24] = 1
    board.checkers['white'] = [3, 19, 22, 24]
    board.rolls = (1, 5)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 2
    assert ((19, 20), (20, 25)) in moves
    assert ((22, 23), (19, 24)) in moves
