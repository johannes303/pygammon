import os
import pprint
from time import sleep

from state import state
from state import black, white
from board_drawer import draw
from functions import roll_dice
from functions import get_rolls
from random_agent import RandomAgent
from pubeval_agent import PubevalAgent
from human_agent import HumanAgent


def main():
    os.system('clear')
    pp = pprint.PrettyPrinter(indent=4)
    #agent = RandomAgent()
    agent = PubevalAgent()
    human = HumanAgent()
    turn = white
    dice = roll_dice()
    print(dice)
    game = state(dice=dice, turn=turn)
    draw(game)
    #print(game.points)
    i = 0
    while not game.is_won():
        #print('***************')
        #print(i)
        #print(game.points)
        #print(game.checkers)
        #print(game.turn)
        turn = switch_player(turn)
        dice = roll_dice()
        #print('dice1')
        #r1 = input()
        #print('dice1')
        #r2 = input()
        #dice = (int(r1), int(r2))
        rolls = get_rolls(dice)
        game.turn = turn
        game.rolls = rolls
        states = game.valid_states()
        #print('****************')
        print(dice)
        #tmp = set()
        if turn == white:
            next_state = agent.get_action(states.values())
            #tmp.add(next_state)
        else:
            if len(states.keys()) == 0:
                next_state = None
            else:
                next_state = human.get_action(states.items())
        if next_state is not None:
            game = next_state
        draw(game)
        i += 1
        #sleep(1)


def switch_player(turn):
    if turn is white:
        return black
    else:
        return white


if __name__ == '__main__':
    main()
