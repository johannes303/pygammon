import pytest

from state import black


def test_65_and_all_at_home(empty_board):
    board = empty_board
    board.points[6] = -1
    board.points[5] = -1
    board.checkers['black'] = [2, 6, 5]
    board.rolls = (6, 5)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 2
    assert ((6, 0), (5, 0)) in moves
    assert ((6, 1), (5, 0)) in moves


def test_63_half_at_home(empty_board):
    board = empty_board
    board.points[9] = -1
    board.points[6] = 1
    board.checkers['white'] = [1, 6]
    board.checkers['black'] = [1, 9]
    board.rolls = (6, 3)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 2
    assert ((9, 6), (6, 0)) in moves
    assert ((9, 3), (3, 0)) in moves


def test_41_when_checker_on_6(empty_board):
    board = empty_board
    board.points[1] = -1
    board.points[3] = -1
    board.points[6] = -1
    board.checkers['black'] = [3, 6, 3, 1]
    board.rolls = (4, 1)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 2
    assert ((6, 2), (3, 2)) in moves
    assert ((6, 2), (2, 1)) in moves
