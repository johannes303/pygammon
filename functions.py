import numpy as np


def roll_dice():
    return (np.random.randint(1, 7), np.random.randint(1, 7))


def get_rolls(dice):
    result = dice
    if dice[0] == dice[1]:
        result += result
    return result
