import numpy as np


class PubevalAgent():

    def __init__(self):
        self.name = 'Pubeval'
        self.wr = np.empty(122, dtype=np.float64)
        self.wc = np.empty(122, dtype=np.float64)
        self.initialize_pubeval_arrays(self.wr, 'WT.race')
        self.initialize_pubeval_arrays(self.wc, 'WT.cntc')

    def get_action(self, moves):
        best_action = None
        best = -99999999.9
        for move in moves:
            state = self.extract_pubeval_array(move)
            score = self.pubeval(False, state)
            if score > best:
                best = score
                best_action = move
        return best_action

    def pubeval(self, race, state):
        if state[26] == 15:
            return 99999999.9
        x = self.setX(state)
        score = 0.0
        if race:
            for i in range(122):
                score += self.wr[i] * x[i]
        else:
            for i in range(122):
                score += self.wc[i] * x[i]
        return score

    def setX(self, state):
        x = np.zeros((122), dtype=np.float64)
        for j in range(1, 25):
            jm1 = j - 1
            n = state[25 - j]
            if n != 0:
                if n == -1:
                    x[5 * jm1 + 0] = 1.0
                if n == 1:
                    x[5 * jm1 + 1] = 1.0
                if n >= 2:
                    x[5 * jm1 + 2] = 1.0
                if n == 3:
                    x[5 * jm1 + 3] = 1.0
                if n >= 4:
                    x[5 * jm1 + 4] = (float)(n - 3) / 2.0
        x[120] = -(float)(state[0]) / 2.0
        x[121] = (float)(state[26]) / 15.0
        return x

    def extract_pubeval_array(self, state):
        result = np.zeros((28), dtype=np.int8)
        for i, _ in enumerate(state.points):
            if i == 0 or i == 25:
                result[i] = _
            if i > 0 and i < 25:
                result[i] = -1 * _
        result[26] = state.number_of_checkers_at_home('black')
        result[27] = -state.number_of_checkers_at_home('white')
        return result

    def initialize_pubeval_arrays(self, arr, filename):
        with open(filename) as fp:
            for i, line in enumerate(fp):
                value = float(line.strip())
                arr[i] = value
