class HumanAgent():

    def __init__(self):
        self.name = 'Human'

    def get_action(self, moves):
        choices = []
        i = 0
        for k, v in moves:
            print(f'{i}: {k}')
            choices.append(v)
            i += 1
        choice = int(input())
        return choices[choice]
