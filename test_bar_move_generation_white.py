import pytest

from state import white


def test_reenter_with_two_checkers_and_nothings_blocked(empty_board):
    board = empty_board
    board.points[0] = 2
    board.checkers['white'] = [1, 0]
    board.rolls = (5, 1)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((0, 5), (0, 1)) in moves


def test_reenter_with_two_checkers_and_one_blocked(empty_board):
    board = empty_board
    board.points[0] = 2
    board.points[2] = -2
    board.checkers['white'] = [1, 0]
    board.checkers['black'] = [1, 2]
    board.rolls = (2, 3)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((0, 3), ) in moves


def test_reenter_with_two_checkers_and_3rd_move_blocked(empty_board):
    board = empty_board
    board.points[0] = 2
    board.points[2] = -2
    board.points[8] = -2
    board.checkers['white'] = [1, 0]
    board.checkers['black'] = [2, 8, 2]
    board.rolls = (4, 4, 4, 4)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((0, 4), (0, 4)) in moves


def test_reenter_with_three_checkers_and_4th_move_blocked(empty_board):
    board = empty_board
    board.points[0] = 3
    board.points[2] = -2
    board.points[6] = -2
    board.checkers['white'] = [1, 0]
    board.checkers['black'] = [2, 6, 2]
    board.rolls = (3, 3, 3, 3)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((0, 3), (0, 3), (0, 3)) in moves
