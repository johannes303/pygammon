import random


class RandomAgent():

    def __init__(self):
        self.name = 'Random'

    def get_action(self, moves):
        return random.choice(list(moves)) if moves else None
