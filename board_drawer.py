def draw(board):
    white = board.points[0]
    print(f'white bar: {white}')
    black = -board.points[25]
    print(f'black bar: {black}')
    result = draw_board_heading()
    result += draw_board_upper_half(board)
    result += draw_bar()
    result += draw_board_lower_half(board)
    result += draw_board_bottom()
    print(result)


def draw_board_upper_half(board):
    result = ''
    for row in range(1, 6):
        result += '|'
        result += draw_quadrant(board, row, 13, 19, 1)
        result += '|   | '
        result += draw_quadrant(board, row, 19, 25, 1)
        result += '|\n'
    return result


def draw_board_lower_half(board):
    result = ''
    for row in range(5, 0, -1):
        result += '|'
        result += draw_quadrant(board, row, 12, 6, -1)
        result += '|   | '
        result += draw_quadrant(board, row, 6, 0, -1)
        result += '|\n'
    return result


def draw_quadrant(board, row, start, end, steps):
    result = ''
    for i in range(start, end, steps):
        result += draw_point(board.points[i], row)
    return result


def draw_point(board_val, row):
    if board_val == 0:
        return '   '

    result = ''
    if row == 5:
        if board_val < 0:
            if board_val < -5:
                result += ' ' + str(-board_val) + ' '
            elif board_val == -5:
                result += ' X '
            else:
                result += '   '
        else:
            if board_val > 5:
                result += ' ' + str(board_val) + ' '
            elif board_val == 5:
                result += ' O '
            else:
                result += '   '
    else:
        if board_val < 0:
            if board_val > -row:
                result += '   '
            else:
                result += ' X '
        else:
            if board_val < row:
                result += '   '
            else:
                result += ' O '
    return result


def draw_bar():
    return '|                  |BAR|                   |\n'


def draw_board_heading():
    return '+13-14-15-16-17-18-------19-20-21-22-23-24-+\n'


def draw_board_bottom():
    return '+12-11-10--9--8--7--------6--5--4--3--2--1-+\n'
