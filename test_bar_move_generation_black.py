import pytest

from state import black


def test_reenter_when_both_dice_blocked(empty_board):
    board = empty_board
    board.points[19] = 2
    board.points[24] = 3
    board.points[25] = -1
    board.checkers['black'] = [1, 25]
    board.checkers['white'] = [2, 19, 24]
    board.rolls = (6, 1)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 0


def test_reenter_with_two_checkers_and_nothings_blocked(empty_board):
    board = empty_board
    board.points[25] = -2
    board.checkers['black'] = [1, 25]
    board.rolls = (2, 3)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((25, 23), (25, 22)) in moves


def test_reenter_with_one_checker_when_one_die_blocked(empty_board):
    board = empty_board
    board.points[20] = 2
    board.points[24] = 3
    board.points[25] = -1
    board.checkers['black'] = [1, 25]
    board.checkers['white'] = [2, 20, 24]
    board.rolls = (5, 4)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((25, 21), (21, 16)) in moves


def test_reenter_with_two_checkers_when_one_die_blocked(empty_board):
    board = empty_board
    board.points[20] = 3
    board.points[21] = 2
    board.points[25] = -2
    board.checkers['black'] = [1, 25]
    board.checkers['white'] = [2, 20, 21]
    board.rolls = (6, 5)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((25, 19), ) in moves


def test_reenter_with_doubles(empty_board):
    board = empty_board
    board.points[19] = 2
    board.points[23] = 2
    board.points[24] = 2
    board.points[25] = -4
    board.checkers['black'] = [1, 25]
    board.checkers['white'] = [3, 19, 23, 24]
    board.rolls = (5, 5, 5, 5)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((25, 20), (25, 20), (25, 20), (25, 20)) in moves
