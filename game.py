from state import state
from state import black, white
from functions import roll_dice
from functions import get_rolls


class game:

    def __init__(self, players):
        self.players = players
        self.state = state()

    def get_first_player(self):
        dice = roll_dice()
        while dice[0] == dice[1]:
            dice = roll_dice()
        self.rolls = get_rolls(dice)
        player_num = 0
        if dice[1] > dice[0]:
            player_num = 1
        return player_num

    def play(self):
        print(self.state)
        print(type(self.state))
        print(self.state.is_over())
        player_num = self.get_first_player()
        while not self.state.is_over():
            self.next_step(self.players[player_num])
            player_num = (player_num + 1) % 2
        return self.state.winner()

    def next_step(self, player):
        self.take_turn(player)

    def take_turn(self, player):
        moves = self.state.valid_states()
        move = player.get_action(moves)
        if move:
            self.state = move
