from game import game
from random_agent import RandomAgent

players = [RandomAgent(), RandomAgent()]


def run():
    g = game(players)
    g.play()


if __name__ == '__main__':
    run()
