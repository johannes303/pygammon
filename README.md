PyGammon
========

migrated to https://gitlab.com/johannes303


Goal
----

Topic of my Bachelor's thesis. This time try out some deep
learning stuff. Need better hardware though ;)

Status
------

This time I am using Python. For the UI I am going to use Electron.

As deep learning framework I am currently evaluating Pytorch, Keras and plain TensorFlow. 
TensorFlow has currently some issues with latest Python version though.

Status of project is the same as in my thesis but without a UI.
And there is much debug logging.

Coding was done in TDD using PyTest this time.
And I tried out Pipenv instead of pip.

TODO
----

- Instructions on how to start a match vs current agent
- improve algorithm for move generation (performance and remove transpositions)
- UI
- Framework for a backgammon tournament for the agent to play matches against
  random play in order to train the neural nets
