import pytest

from state import black


def test_52(empty_board):
    board = empty_board
    board.points[12] = -2
    board.points[21] = -1
    board.checkers['black'] = [2, 21, 12]
    board.rolls = (5, 2)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 5
    assert ((21, 16), (16, 14)) in moves
    assert ((21, 16), (12, 10)) in moves
    assert ((12, 7), (21, 19)) in moves
    assert ((12, 7), (12, 10)) in moves
    assert ((12, 7), (7, 5)) in moves


def test_52_when_he_can_capture(empty_board):
    board = empty_board
    board.points[12] = -2
    board.points[16] = 1
    board.points[21] = -1
    board.checkers['black'] = [2, 21, 12]
    board.checkers['white'] = [1, 16]
    board.rolls = (5, 2)
    board.turn = black
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 6
    assert ((21, 16), (16, 14)) in moves
    assert ((21, 19), (19, 14)) in moves
    assert ((21, 16), (12, 10)) in moves
    assert ((12, 7), (21, 19)) in moves
    assert ((12, 7), (12, 10)) in moves
    assert ((12, 7), (7, 5)) in moves


def test_must_use_higest_dice(empty_board):
    board = empty_board
    board.points[13] = -1
    board.points[6] = 2
    board.checkers['white'] = [1, 6]
    board.checkers['black'] = [1, 13]
    board.turn = black
    board.rolls = (6, 1)
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((13, 7)) in moves
    assert ((13, 12)) not in moves
