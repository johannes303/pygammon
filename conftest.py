import pytest
import numpy as np

from state import state


@pytest.fixture
def initial_board():
    return state()


@pytest.fixture
def empty_board():
    points = np.zeros((26), dtype=np.int8)
    checkers = {'white': [0], 'black': [0]}
    return state(points=points, checkers=checkers)
