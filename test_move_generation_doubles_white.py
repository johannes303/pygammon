import pytest

from state import white


def test_22(empty_board):
    board = empty_board
    board.points[1] = 1
    board.points[8] = 1
    board.checkers['white'] = [2, 1, 8]
    board.rolls = (2, 2, 2, 2)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 5
    assert ((1, 3), (3, 5), (5, 7), (7, 9)) in moves
    assert ((1, 3), (3, 5), (5, 7), (8, 10)) in moves
    assert ((1, 3), (3, 5), (8, 10), (10, 12)) in moves
    assert ((1, 3), (8, 10), (10, 12), (12, 14)) in moves
    assert ((8, 10), (10, 12), (12, 14), (14, 16)) in moves
