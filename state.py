from collections import namedtuple

import numpy as np

from functions import roll_dice
from functions import get_rolls

OFFSET = 25
Player = namedtuple('Player', ['color', 'value', 'home', 'bar', 'home_begin', 'opponent'])
white = Player('white', 1, 25, 0, 18, 'black')
black = Player('black', -1, 0, 25, 6, 'white')


class state():

    def __init__(self, points=None, checkers=None, dice=None, turn=None):
        if points is None:
            self.points = np.zeros((26), dtype=np.int8)
            self.points[1] = 2
            self.points[6] = -5
            self.points[8] = -3
            self.points[12] = 5
            self.points[13] = -5
            self.points[17] = 3
            self.points[19] = 5
            self.points[24] = -2
        else:
            self.points = points
        if checkers is None:
            self.checkers = {
                'white': [4, 1, 12, 17, 19],
                'black': [4, 24, 13, 8, 6]
            }
        else:
            self.checkers = checkers
        if dice is None:
            dice = roll_dice()
            self.rolls = get_rolls(dice)
        else:
            self.rolls = get_rolls(dice)
        if turn is None:
            self.turn = white
        else:
            self.turn = turn
        self.partials = {}
        self.boards = {}

    def __eq__(self, other):
        return np.array_equal(self.points, other.points)

    def __repr__(self):
        return str(self.points) + str(self.checkers)

    def copy(self):
        points = np.copy(self.points)
        checkers = {
            'white': list(self.checkers['white']),
            'black': list(self.checkers['black'])
        }
        return state(points=points, checkers=checkers, turn=self.turn)

    def is_won(self):
        return self.checkers['white'] == [0] or self.checkers['black'] == [0]

    def winner(self):
        return white if self.checkers['white'] == [0] else black

    def is_over(self):
        if self.checkers['white'] == [0] or self.checkers['black'] == [0]:
            return True
        return False

    def is_race(self):
        return self.checkers['white'][1] > self.checkers['black'][1]

    def valid_states(self):
        clone = self.copy()
        self.generate_moves(clone, self.rolls, ())
        if not self.rolls[0] == self.rolls[1]:
            self.generate_moves(clone, (self.rolls[1], self.rolls[0]), ())
        if len(self.boards.keys()) > 0:
            return self.boards
        to_delete = list()
        longest_move = 0
        for k in self.partials.keys():
            move_length = len(k)
            if move_length > longest_move:
                longest_move = move_length
                to_delete.append(k)
        for _ in to_delete:
            if len(_) < longest_move:
                del(self.partials[_])
        return self.partials

    def generate_moves(self, state, rolls, move):
        if len(rolls) == 0:
            if state not in self.boards.values():
                self.boards[move] = state
            return
        if state not in self.partials.values() and move != ():
            self.partials[move] = state
        r, rs = rolls[0], rolls[1:]
        for src in state.all_checkers():
            dest = state.dest(src, r)
            next_move = (src, dest)
            if state.legal_move(next_move):
                clone = state.copy()
                clone.apply_move(next_move)
                self.generate_moves(clone, rs, move + (next_move, ))

    # TODO for black reverse order?!
    def all_checkers(self):
        color = getattr(self.turn, 'color')
        for checker in self.checkers[color][1:]:
            yield checker

    def number_of_checkers_at_home(self, color=None):
        no_of_checkers = self.number_of_checkers(color)
        if no_of_checkers < 0:
            no_of_checkers *= -1
        return 15 - no_of_checkers

    def number_of_checkers(self, color=None):
        result = 0
        if color is None:
            color = getattr(self.turn, 'color')
        value = getattr(self.turn, 'value')
        for i in self.checkers[color][1:]:
            result += self.points[i] * value
        return result

    def pip_count(self, player):
        color = getattr(self.turn, 'color')
        value = getattr(self.turn, 'value')
        home = getattr(self.turn, 'home')
        result = 0
        for i in self.checkers[color][1:]:
            result += (home - i) * self.points[i] * value
        return result

    def update_checker_positions(self, src, dest):
        color = getattr(self.turn, 'color')
        home = getattr(self.turn, 'home')
        arr = self.checkers[color]
        if self.points[src] == 0:
            self.remove_checker_from_positions(color, src)
        if dest not in arr[1:] and dest != home:
            self.add_checler_to_positions(dest)

    def add_checler_to_positions(self, dest):
        color = getattr(self.turn, 'color')
        value = getattr(self.turn, 'value')
        arr = self.checkers[color]
        arr[0] += 1
        pos = 1
        for elem in arr[1:]:
            if elem * value > dest * value:
                break
            pos += 1
        arr[pos:pos] = [dest]

    def remove_checker_from_positions(self, color, src):
        arr = self.checkers[color]
        if arr[0] == src:
            arr[0] -= 1
            arr.remove(src)
        else:
            arr.remove(src)
            arr[0] -= 1

    def put_checker_on_bar(self):
        opponent = getattr(self.turn, 'opponent')
        bar_opponent = getattr(self.turn, 'home')
        value = getattr(self.turn, 'value')
        self.points[bar_opponent] -= value
        if bar_opponent not in self.checkers[opponent][1:]:
            self.checkers[opponent][0] += 1
            self.checkers[opponent][1:1] = [bar_opponent]

    def apply_moves(self, moves):
        for move in moves:
            self.apply_move(move)

    def apply_move(self, move):
        value = getattr(self.turn, 'value')
        opponent = getattr(self.turn, 'opponent')
        home = getattr(self.turn, 'home')
        src, dest = move
        self.points[src] -= value
        if dest != home:
            self.points[dest] += value
        self.update_checker_positions(src, dest)
        if self.points[dest] == 0 and dest != home:
            self.points[dest] += value
            self.remove_checker_from_positions(opponent, dest)
            self.put_checker_on_bar()

    def legal_move(self, move):
        value = getattr(self.turn, 'value')
        home = getattr(self.turn, 'home')
        bar = getattr(self.turn, 'bar')
        src, dest = move
        empty_bar = True
        if src is not bar:
            empty_bar = self.empty_bar()
        can_bear_off = True
        if value * (home - dest) <= 0:
            can_bear_off = self.can_bear_off(src)
        point_blocked = self.point_blocked(dest)
        return empty_bar and can_bear_off and not point_blocked

    def can_bear_off(self, src):
        all_at_home = self.all_at_home()
        last_checker = self.last_checker(src)
        return all_at_home and last_checker

    def last_checker(self, src):
        color = getattr(self.turn, 'color')
        value = getattr(self.turn, 'value')
        return self.checkers[color][1] * value >= src * value

    def all_at_home(self):
        color = getattr(self.turn, 'color')
        value = getattr(self.turn, 'value')
        home_begin = getattr(self.turn, 'home_begin')
        return self.checkers[color][1] * value >= home_begin * value

    def point_blocked(self, point):
        value = getattr(self.turn, 'value')
        return self.points[point] * value < -1

    def empty_bar(self):
        bar = getattr(self.turn, 'bar')
        return self.points[bar] == 0

    def dest(self, src, roll):
        value = getattr(self.turn, 'value')
        home = getattr(self.turn, 'home')
        dest = src + roll * value
        if dest * value > home * value:
            dest = home
        return dest
