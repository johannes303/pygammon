from state import white


def test_31(empty_board):
    board = empty_board
    board.points[1] = 2
    board.checkers['white'] = [1, 1]
    board.rolls = (3, 1)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 2
    assert ((1, 4), (1, 2)) in moves
    assert ((1, 4), (4, 5)) in moves


def test_31_when_he_can_capture(empty_board):
    board = empty_board
    board.points[1] = 2
    board.points[2] = -1
    board.checkers['white'] = [1, 1]
    board.checkers['black'] = [1, 2]
    board.rolls = (3, 1)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 3
    assert ((1, 4), (1, 2)) in moves
    assert ((1, 4), (4, 5)) in moves
    assert ((1, 2), (2, 5)) in moves


def test_42_when_6_is_blocked(empty_board):
    board = empty_board
    board.points[1] = 2
    board.points[3] = -1
    board.points[7] = -2
    board.checkers['white'] = [1, 1]
    board.checkers['black'] = [2, 7, 3]
    board.rolls = (4, 2)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((1, 5), (1, 3)) in moves


def test_53(empty_board):
    board = empty_board
    board.points[1] = 1
    board.points[8] = 1
    board.points[6] = -1
    board.checkers['white'] = [2, 1, 8]
    board.checkers['black'] = [1, 6]
    board.rolls = (5, 3)
    board.turn = white
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 5
    assert ((1, 6), (6, 9)) in moves
    assert ((1, 6), (8, 11)) in moves
    assert ((8, 13), (1, 4)) in moves
    assert ((8, 13), (13, 16)) in moves
    assert ((1, 4), (4, 9)) in moves


def test_must_use_all_dice_if_possible(empty_board):
    board = empty_board
    board.points[11] = 1
    board.points[12] = 1
    board.points[18] = -2
    board.points[19] = -2
    board.checkers['white'] = [2, 11, 12]
    board.checkers['black'] = [2, 19, 18]
    board.turn = white
    board.rolls = (6, 1)
    states = board.valid_states()
    moves = states.keys()
    assert len(moves) == 1
    assert ((11, 17), (12, 13)) in moves
    assert ((11, 12), ) not in moves
