from functions import get_rolls


def test_double_6_returns_4_rolls():
    rolls = get_rolls([6, 6])
    assert len(rolls) == 4
    assert rolls == [6, 6, 6, 6]


def test_12_returns_2_rolls():
    rolls = get_rolls([1, 2])
    assert len(rolls) == 2
    assert rolls == [1, 2]
